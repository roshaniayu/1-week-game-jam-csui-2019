extends Area2D

export (String) var scene_name = "Stage_1"
export (String) var scene_name_2 = "Stage_2"
export (String) var scene_name_3 = "Stage_3"

func _on_Area2D_body_entered(body):
	if body.get_name() == "Clown":
		Global.live -= 1
		get_tree().change_scene(str("res://Scenes/" + scene_name + ".tscn"))

func _on_Area2D2_body_entered(body):
	if body.get_name() == "Clown_2":
		Global.live -= 1
		Global.flag_died_stage_2 += 1
		get_tree().change_scene(str("res://Scenes/" + scene_name_2 + ".tscn"))

func _on_Area2D3_body_entered(body):
	if body.get_name() == "Clown":
		Global.live -= 1
		Global.flag_died_stage_3 += 1
		get_tree().change_scene(str("res://Scenes/" + scene_name_3 + ".tscn"))