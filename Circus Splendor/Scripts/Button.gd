extends LinkButton

export(String) var scene_to_load
	
func _on_Start_Game_pressed():
	get_node("../../AnimationPlayer").play("Transition")
	get_node("../../Keyboard").show()
	get_node("../../How_To_Play").show()
	get_node("../../Press_Up").show()
	yield(get_tree().create_timer(6), "timeout")
	get_node("../../Keyboard").hide()
	get_node("../../How_To_Play").hide()
	get_node("../../Press_Up").hide()
	get_node("../../Stage").show()
	get_node("Clapping").play()
	yield(get_tree().create_timer(6), "timeout")
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Select_Stage_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	
func _on_Continue_pressed():
	Global.score = 0
	Global.live = 3
	Global.stage = 1
	Global.flag_died_stage_2 = 0
	Global.flag_died_stage_3 = 0
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Quit_pressed():
	get_tree().quit()

func _on_Stage_1_pressed():
	get_node("../../AnimationPlayer").play("Transition")
	get_node("../../Keyboard").show()
	get_node("../../How_To_Play").show()
	get_node("../../Press_Up").show()
	yield(get_tree().create_timer(6), "timeout")
	get_node("../../Keyboard").hide()
	get_node("../../How_To_Play").hide()
	get_node("../../Press_Up").hide()
	get_node("../../Stage").show()
	get_node("Clapping").play()
	yield(get_tree().create_timer(6), "timeout")
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Stage_2_pressed():
	Global.stage = 2
	get_node("../../AnimationPlayer").play("Transition")
	get_node("../../Keyboard").show()
	get_node("../../How_To_Play").show()
	get_node("../../Press_Up").show()
	yield(get_tree().create_timer(6), "timeout")
	get_node("../../Keyboard").hide()
	get_node("../../How_To_Play").hide()
	get_node("../../Press_Up").hide()
	get_node("../../Stage").show()
	get_node("Clapping").play()
	yield(get_tree().create_timer(6), "timeout")
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Stage_3_pressed():
	Global.stage = 3
	get_node("../../AnimationPlayer").play("Transition")
	get_node("../../Keyboard").show()
	get_node("../../How_To_Play").show()
	get_node("../../Press_Up").show()
	yield(get_tree().create_timer(6), "timeout")
	get_node("../../Keyboard").hide()
	get_node("../../How_To_Play").hide()
	get_node("../../Press_Up").hide()
	get_node("../../Stage").show()
	get_node("Clapping").play()
	yield(get_tree().create_timer(6), "timeout")
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Back_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))