extends Camera2D

var off_set_loc = 0
var off_set_loc_2 = 0
var off_set_loc_3 = 0
var speed = 270
var speed_2 = 370
var speed_3 = 570

func _ready():
	set_process(true)

func _process(delta):
	off_set_loc += speed * delta
	off_set_loc_2 += speed_2 * delta
	off_set_loc_3 += speed_3 * delta
	if Global.stage == 3:
		set_offset(Vector2(off_set_loc_3,0))
	elif Global.stage == 2:
		set_offset(Vector2(off_set_loc_2,0))
	else:
		set_offset(Vector2(off_set_loc,0))
	set_limit(1,-1)