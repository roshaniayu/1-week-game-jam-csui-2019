extends KinematicBody2D

export (int) var GRAVITY = 1200
export (int) var jump_speed = -550

const UP = Vector2(0,-1)

var velocity = Vector2()
var jump = false

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		jump = true
		velocity.y = jump_speed
		get_node("Jump").play()
	elif is_on_floor():
		jump = false

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if jump == false:
		if Global.stage == 3:
			$AnimatedSprite.play("Stand")
		else:
			$AnimatedSprite.play("Walk")
	elif jump == true:
		$AnimatedSprite.play("Jump")
	position.x = clamp(position.x, 0, 1024)