extends Area2D

export (String) var scene_name = "Stage_2"
export (String) var scene_name_2 = "Stage_3"
export (String) var scene_name_3 = "End"
	
func _on_Area2D_body_entered(body):
	if body.get_name() == "Clown":
		if Global.live == 3:
			Global.score += 5000
		if Global.live == 2:
			Global.score += 1500
		if Global.live == 1:
			Global.score += 200
		Global.stage += 1
		get_node("Clapping").play()
		get_node("../../../../AnimationPlayer").play("Transition")
		get_node("../../../Stage").show()
		get_node("../../../Stage2").hide()
		yield(get_tree().create_timer(6), "timeout")
		get_tree().change_scene(str("res://Scenes/" + scene_name + ".tscn"))

func _on_Area2D2_body_entered(body):
	if body.get_name() == "Clown_2":
		if Global.flag_died_stage_2 == 0:
			Global.score += 5000
		if Global.flag_died_stage_2 == 1:
			Global.score += 1500
		if Global.flag_died_stage_2 == 2:
			Global.score += 200
		Global.stage += 1
		get_node("Clapping").play()
		get_node("../../../../AnimationPlayer").play("Transition")
		get_node("../../../Stage").show()
		get_node("../../../Stage2").hide()
		yield(get_tree().create_timer(6), "timeout")
		get_tree().change_scene(str("res://Scenes/" + scene_name_2 + ".tscn"))

func _on_Area2D3_body_entered(body):
	if body.get_name() == "Clown":
		if Global.flag_died_stage_3 == 0:
			Global.score += 5000
		if Global.flag_died_stage_3 == 1:
			Global.score += 1500
		if Global.flag_died_stage_3 == 2:
			Global.score += 200
		get_node("Clapping").play()
		yield(get_tree().create_timer(6), "timeout")
		get_tree().change_scene(str("res://Scenes/" + scene_name_3 + ".tscn"))