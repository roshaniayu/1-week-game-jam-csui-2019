extends KinematicBody2D

export (int) var GRAVITY = 1200
export (int) var jump_speed = -550

const UP = Vector2(0,-1)

var velocity = Vector2()

func _ready():
	pass

func _physics_process(delta):
	velocity.y += delta * GRAVITY

func _process(delta):
	$AnimatedSprite.play("Run")