extends ParallaxBackground

export (String) var scene_name = "End"

func _ready():
	pass

func _process(delta):
	if Global.live == 2:
		get_node("Live3").hide()
	if Global.live == 1:
		get_node("Live2").hide()
		get_node("Live3").hide()
	if Global.live == 0:
		get_tree().change_scene(str("res://Scenes/" + scene_name + ".tscn"))