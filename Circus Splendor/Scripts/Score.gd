extends Label

func _process(delta):
	if Global.score == 0:
		self.text = "0000000"
	elif Global.score >= 10000:
		self.text = "00" + str(Global.score)
	elif Global.score >= 1000:
		self.text = "000" + str(Global.score)
	else:
		self.text = "0000" + str(Global.score)
